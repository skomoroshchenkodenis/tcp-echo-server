#include "ringexecutor.hpp"

#include <stdexcept>
#include <array>
#include <iostream>

#include "accepttask.hpp"
#include "readtask.hpp"
#include "waittask.hpp"
#include "writetask.hpp"
#include "shutdowntask.hpp"

RingExecutor::RingExecutor(unsigned int entries) {
    int status = io_uring_queue_init(entries, &m_Ring, 0);
    if(status != 0) {
        throw std::runtime_error("Cannot init uring queue");
    }
}

RingExecutor::~RingExecutor() {
    io_uring_queue_exit(&m_Ring);
}

void RingExecutor::run() {
    io_uring_cqe *ready;
    constexpr int readyTasksSize = 512;
    while(true) {
        io_uring_submit(&m_Ring);
        int ret = io_uring_wait_cqe(&m_Ring, &ready);
        if(ret != 0) {
            throw std::runtime_error("Error waiting cqe");
        }
        std::array<io_uring_cqe*, readyTasksSize> readyTasks;
        int readyCount = io_uring_peek_batch_cqe(&m_Ring, readyTasks.data(), readyTasksSize);
        for(int i = 0; i < readyCount; ++i) {
            auto cqe = readyTasks[i];
            auto task = reinterpret_cast<SubmissionTaskBase*>(io_uring_cqe_get_data(cqe));
            task->onFinished(cqe);
            delete task;
        }
        io_uring_cq_advance(&m_Ring, readyCount);
    }
}

void RingExecutor::createAcceptTask(int socket) {
    auto *task = new AcceptTask(this, socket);
}

void RingExecutor::createReadTask(int socket) {
    auto *task = new ReadTask(this, socket);
}

void RingExecutor::createWaitTask(int socket, std::string&& message, int actualSize) {
    auto *task = new WaitTask(this, socket, 5, std::move(message), actualSize);
}

void RingExecutor::createWriteTask(int socket, std::string&& message, int actualSize) {
    auto *task = new WriteTask(this, socket, std::move(message), actualSize);
}

void RingExecutor::createShutdownTask(int socket) {
    auto *task = new ShutdownTask(this, socket);
}

io_uring* RingExecutor::getRing() noexcept {
    return &m_Ring;
}