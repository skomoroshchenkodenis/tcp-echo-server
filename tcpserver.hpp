#pragma once

#include "ringexecutor.hpp"

class TcpServer {
public:
    TcpServer(int port = 7000);
    ~TcpServer();
private:
    void run();
    void doAccept();
    void doRead(io_uring_cqe *cqe);
private:
    int m_ServerSocket;
    RingExecutor m_Executor;
};