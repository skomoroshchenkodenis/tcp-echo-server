#include "waittask.hpp"

WaitTask::WaitTask(RingExecutor *parent, int clientSocket, int timeoutSec, std::string&& message, int actualSize) 
        : SubmissionTaskBase(parent), m_ClientSocket(clientSocket), m_Message(std::move(message)), m_ActualSize(actualSize) {
    m_Timeout.tv_sec = timeoutSec;        
    io_uring_prep_timeout(m_Sqe, &m_Timeout, 0, 0);
    io_uring_sqe_set_data(m_Sqe, this);
}

void WaitTask::onFinished(io_uring_cqe *cqe) {
    m_Parent->createWriteTask(m_ClientSocket, std::move(m_Message), m_ActualSize);   
}