#pragma once

#include "submissiontaskbase.hpp"

#include <string>

class ReadTask : public SubmissionTaskBase {
public:
    ReadTask(RingExecutor *parent, int clientSocket, int maxBufferSize = 1024);

    void onFinished(io_uring_cqe *cqe) override;
private:
    int m_ClientSocket;
    std::string m_Message;
};