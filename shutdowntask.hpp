#pragma once

#include "submissiontaskbase.hpp"

class ShutdownTask : public SubmissionTaskBase {
public:
    ShutdownTask(RingExecutor *parent, int clientSocket) 
         : SubmissionTaskBase(parent), m_ClientSocket(clientSocket)  {
        io_uring_prep_shutdown(m_Sqe, m_ClientSocket, SHUT_RDWR);
        io_uring_sqe_set_data(m_Sqe, this);
    }

    void onFinished(io_uring_cqe *cqe) override {
        
    }
private:
    int m_ClientSocket;
};