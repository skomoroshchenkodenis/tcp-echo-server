#pragma once

#include "submissiontaskbase.hpp"

#include <netinet/in.h>
#include <sys/socket.h>

class RingExecutor;

class AcceptTask : public SubmissionTaskBase {
public:
    AcceptTask(RingExecutor *parent, int serverSocket);
    void onFinished(io_uring_cqe *cqe) override;
private:
    int m_ServerSocket;
    sockaddr_in m_Sockaddr;
    unsigned int m_SockaddrSize;
};
