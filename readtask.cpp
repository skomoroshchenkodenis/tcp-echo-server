#include "readtask.hpp"

#include <netinet/in.h>
#include <sys/socket.h>

ReadTask::ReadTask(RingExecutor *parent, int clientSocket, int maxBufferSize) 
        : SubmissionTaskBase(parent), m_ClientSocket(clientSocket) {
    m_Message.resize(maxBufferSize);
    io_uring_prep_read(m_Sqe, m_ClientSocket, m_Message.data(), m_Message.size(), 0);
    io_uring_sqe_set_data(m_Sqe, this);
}

void ReadTask::onFinished(io_uring_cqe *cqe) {
    int bytesRead = cqe->res;
    if(bytesRead <= 0) {
        m_Parent->createShutdownTask(m_ClientSocket);
    } else {
        m_Parent->createWaitTask(m_ClientSocket, std::move(m_Message), bytesRead);
        m_Parent->createReadTask(m_ClientSocket);
    }
    
}