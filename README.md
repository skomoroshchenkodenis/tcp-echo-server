# tcp-echo-server
Async tcp echo server by liburing
# Build instruction
## Requirements
- C++17 compatible compiler (tested on G++)
- CMake
- Git
## Build and run
`git clone https://gitlab.com/skomoroshchenkodenis/tcp-echo-server.git`

`cmake .`

`make`

`./Tcp_echo_server`
