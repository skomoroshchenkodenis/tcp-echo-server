#include "tcpserver.hpp"

#include <netinet/in.h>
#include <sys/socket.h>
#include <iostream>
#include <cerrno>
#include <cstring>

TcpServer::TcpServer(int port) : m_Executor(512) {
    sockaddr_in serverAddr {AF_INET, htons(port)};
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    m_ServerSocket = socket(AF_INET, SOCK_STREAM, 0);
    int reuseAddr = 1;
    int status = setsockopt(m_ServerSocket, SOL_SOCKET, SO_REUSEADDR, &reuseAddr, sizeof(reuseAddr));
    if(status < 0) {
        std::cout << "Setsockopt error " << status << "\n";
        std::cout << std::strerror(errno) << "\n";
    }
    status = bind(m_ServerSocket, reinterpret_cast<sockaddr*>(&serverAddr), sizeof(serverAddr));
    if(status < 0) {
        std::cout << "Server socket bind error " << status << "\n";
        std::cout << std::strerror(errno) << "\n";
    }
    status = listen(m_ServerSocket, 1024);
    if(status < 0) {
        std::cout << "Server socket listen error " << status << "\n";
    }
    run();
}

TcpServer::~TcpServer() {
    close(m_ServerSocket);
}

void TcpServer::run() {
    doAccept();
    m_Executor.run();
}

void TcpServer::doAccept() {
 /*   sockaddr_in clientAddr;
    unsigned int clientLen = sizeof(clientAddr);
    auto acceptCallback = m_Executor.createAcceptCallback(m_ServerSocket, reinterpret_cast<sockaddr*>(&clientAddr), &clientLen); 
    auto acceptTask = m_Executor.createAcceptTask(m_ServerSocket, reinterpret_cast<sockaddr*>(&clientAddr), &clientLen);
    acceptTask->setCallback([this](io_uring_cqe *cqe){
       std::cout << "Someone connected\n";
       doRead();
       doAccept();
    });*/
    m_Executor.createAcceptTask(m_ServerSocket);
}

void TcpServer::doRead(io_uring_cqe *cqe) {
  //  auto readTask = m_Executor.createReadTask(cqe->res); 
}
