#pragma once

#include "submissiontaskbase.hpp"

class WriteTask : public SubmissionTaskBase {
public:
    WriteTask(RingExecutor *parent, int clientSocket, std::string&& message, int actualSize) 
         : SubmissionTaskBase(parent), m_ClientSocket(clientSocket), m_Message(std::move(message)), m_ActualSize(actualSize) {
        io_uring_prep_send(m_Sqe, m_ClientSocket, m_Message.data(), m_ActualSize, 0);
        io_uring_sqe_set_data(m_Sqe, this);
    }

    void onFinished(io_uring_cqe *cqe) override {

    }
private:
    int m_ClientSocket;
    std::string m_Message;
    int m_ActualSize;
};