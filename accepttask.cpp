#include "accepttask.hpp"

#include <iostream>

#include "ringexecutor.hpp"

AcceptTask::AcceptTask(RingExecutor *parent, int serverSocket) 
        : SubmissionTaskBase(parent), m_ServerSocket(serverSocket), m_SockaddrSize(sizeof(m_Sockaddr)) {
    io_uring_prep_accept(m_Sqe, m_ServerSocket, reinterpret_cast<sockaddr*>(&m_Sockaddr), &m_SockaddrSize, 0);
    io_uring_sqe_set_data(m_Sqe, this);
}

void AcceptTask::onFinished(io_uring_cqe *cqe) {
    m_Parent->createReadTask(cqe->res);
    m_Parent->createAcceptTask(m_ServerSocket);
}
