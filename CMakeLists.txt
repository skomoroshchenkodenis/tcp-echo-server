cmake_minimum_required (VERSION 3.13)
project(Tcp_echo_server)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(SOURCES "main.cpp" "ringexecutor.cpp" "tcpserver.cpp" "accepttask.cpp" "readtask.cpp" "waittask.cpp" )
set(HEADERS "ringexecutor.hpp" "tcpserver.hpp" "submissiontaskbase.hpp" "accepttask.hpp" "readtask.hpp" "waittask.hpp" "writetask.hpp" "shutdowntask.hpp")
add_executable (Tcp_echo_server ${SOURCES} ${HEADERS})

target_link_libraries(Tcp_echo_server uring)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_GNU_SOURCE -luring")