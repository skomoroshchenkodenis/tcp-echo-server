#pragma once

#include <liburing.h>
#include <string>

class RingExecutor {
public:
    RingExecutor(unsigned int entries);
    ~RingExecutor();

    void run();

    void createAcceptTask(int socket);
    void createReadTask(int socket);
    void createWaitTask(int socket, std::string&& message, int actualSize);
    void createWriteTask(int socket, std::string&& message, int actualSize);
    void createShutdownTask(int socket);

    io_uring* getRing() noexcept;

private:
    io_uring m_Ring;
};