#pragma once

#include "submissiontaskbase.hpp"

#include <string>

class WaitTask : public SubmissionTaskBase {
public:
    WaitTask(RingExecutor *parent, int clientSocket, int timeoutSec, std::string&& message, int actualSize);

    void onFinished(io_uring_cqe *cqe) override;
private:
    int m_ClientSocket;
     __kernel_timespec m_Timeout;
    std::string m_Message;
    int m_ActualSize;
};