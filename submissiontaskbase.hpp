#pragma once

#include <liburing.h>

#include "ringexecutor.hpp"

class SubmissionTaskBase {
public:
    
    SubmissionTaskBase(RingExecutor *parent) : m_Parent(parent) {
        m_Sqe = io_uring_get_sqe(m_Parent->getRing());
    }

    virtual ~SubmissionTaskBase() = default;
    virtual void onFinished(io_uring_cqe *cqe) = 0;

protected:
    io_uring_sqe *m_Sqe;
    RingExecutor *m_Parent;
};